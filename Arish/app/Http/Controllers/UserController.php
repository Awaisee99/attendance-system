<?php

namespace App\Http\Controllers;

use App\usertbl;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return usertbl::all();
    }
    public function create(Request $request)
    {
        $usertbl = new usertbl();
       $usertbl->name = $request->input('name');
       $usertbl->email = $request->input('email');
       $usertbl->password = $request->input('password');
       $usertbl->save();
       // $usertbl::create($request->all());
        return  $usertbl;

    }
    public function read($id)
    {

       return usertbl::find($id);

    }
    public function update(Request $request,$id)
    {

        $usertbl= usertbl::find($id);
        if( ($request->input('name') != null) &&  ($request->input('email') == null) && ($request->input('password') == null))
            {
                $usertbl->name = $request->input('name');
            }
        if( ($request->input('name') == null) &&  ($request->input('email') != null) && ($request->input('password') == null))
        {
            $usertbl->email = $request->input('email');
        }
        if( ($request->input('name') == null) &&  ($request->input('email') == null) && ($request->input('password') != null))
        {
            $usertbl->password = $request->input('password');
        }
        if(( $request->input('name') != null) &&  ($request->input('email') != null) && ($request->input('password') == null))
        {
            $usertbl->name = $request->input('name');
            $usertbl->email = $request->input('email');
        }
        if( ($request->input('name') != null) &&  ($request->input('password') != null)  && ($request->input('email') == null))
        {
            $usertbl->name = $request->input('name');
            $usertbl->password = $request->input('password');
        }
        if( ($request->input('email') != null) &&  ($request->input('password') != null) && ($request->input('email') == null))
        {

            $usertbl->email = $request->input('email');
            $usertbl->password = $request->input('password');
        }
        if( ($request->input('email') != null) &&  ($request->input('password') != null) && ($request->input('name') != null))
        {
            $usertbl->name = $request->input('name');
            $usertbl->email = $request->input('email');
            $usertbl->password = $request->input('password');
        }




        $usertbl->save();
        return $usertbl;
    }
    public function delete($id)
    {

        $userdata = usertbl::findorFail($id);
        $userdata->delete();
        return 204;
    }
}
