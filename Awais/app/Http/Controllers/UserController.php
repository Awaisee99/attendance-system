<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    public function all()
    {
        $query = DB::select('select * from usertbls');
        return $query;
    }
    public function search()
    {
        $id=Input::get('id');
        if(!(empty($id))) {
            $query = DB::select('select * from usertbls where id = ?', [$id]);
            if(empty($query))
                return "Record not Found";
            else
                return $query;
        }
        else
            {
                $name=Input::get('name');
                if(!(empty($name))) {
                    $query = DB::select('select * from usertbls where name = ?', [$name]);
                    if (empty($query))
                        return "Record not Found";
                    else
                        return $query;
                                    }
                else
                    return "Record Not Found";

           }


    }
    public function delete()
    {
        $id=Input::get('id');
        if(!(empty($id))) {
            $query = DB::delete('delete from usertbls where id = ?', [$id]);
            if(empty($query))
                return "No Match";
            else
            {
                $query = DB::select('select * from usertbls');
                return $query;
            }
        }
        else
        {
            $name=Input::get('name');
            if(!(empty($name))) {
                $query = DB::delete('delete from usertbls where name = ?', [$name]);
                if (empty($query))
                    return "No Match";
                else
                {
                    $query = DB::select('select * from usertbls');
                    return $query;
                }
            }
            else
                return "Pass Something in parameters with name variable or Id";

        }


    }

}
