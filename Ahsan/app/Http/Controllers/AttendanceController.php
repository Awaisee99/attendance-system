<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\attendance;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function search($id)
    {
       return attendance::find($id);

    }

    public function destroy($id)
    {
        $a=attendance::find($id);
        $a->delete();
        $query=DB::select('select * from attendances');
        return $query;
    }
}
