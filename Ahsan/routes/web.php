<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Resources\User as UserResource;
use  App\Http\Resources\UserCollection;
use App\User;
//use App\Http\Controllers\AttendanceController;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/user', function () {
    return new UserResource(User::find(1));
});

Route::get('/users', function () {
    return new UserCollection(User::all());
});

Route::get('/search/{id}','AttendanceController@search');
Route::get('destroy/{id}','AttendanceController@destroy');